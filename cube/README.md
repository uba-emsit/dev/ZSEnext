# Primary dimensions

## Region ![Region icon](icons/region.jpg)

- Position: 0
- Name: Region
- ID: REGION
- Allow units: FALSE
- [Descriptors](descriptors/regions.csv)

![Region screenshot](screens/region.png)

## Category ![Category icon](icons/category.jpg) / ![Category icon (unused)](icons/category_unused.jpg)

- Position: 1
- Name: Quellgruppe
- ID: CATEGORY
- Allow units: FALSE
- [Descriptors](descriptors/categories.csv)

![Category screenshot](screens/category.png)

## Activity ![Category icon](icons/activity.jpg)

- Position: 2
- Name: Aktivität
- ID: ACTIVITY
- Allow units: FALSE
- [Descriptors](descriptors/activities.csv)

![Activity screenshot](screens/activity.png)

## Type ![Type icon](icons/type.jpg)

- Position: 3
- Name: Wertetyp
- ID: TYPE
- Allow units: TRUE
- [Descriptors](descriptors/types.csv)

![Type screenshot](screens/type.png)

## Substance ![Substance icon](icons/substance.jpg)

- Position: 4
- Name: Schadstoff
- ID: SUBSTANCE
- Allow units: TRUE
- [Descriptors](descriptors/pollutants.csv)

![Substance screenshot](screens/substance.png)

# Secondary (helper) dimensions

No 2nd level dimension allows for units.

| Dimension | Position| Name | ID | Descriptors |
| --- | :---: | --- | --- | --- |
| Fuel | 5 | Brennstoff | FUEL | [List](descriptors/fuels.csv) |
| Power vs. Heat vs. Work | 6 | Energie: Strom/Wärme/Arbeit | POWER_HEAT_WORK | [List](descriptors/power_heat.csv) |
| Firing technology | 7 | Energie: Feuerungstechnik | FIRING_TECH | [List](descriptors/firing_tech.csv) |
| Energy balance flag | 8 | Energie: Bilanzflag | EB_FLAG | [List](descriptors/energy_balance_flag.csv) |
| Energy balance row | 9 | Energie: Bilanzzeile | EB_ROW | [List](descriptors/energy_balance_row.csv) |
| Plant permit (legal framework) | 10 | Energie: Genehmigungsgrundlage | PERMIT | [List](descriptors/permitting.csv) |
| Transport trinity | 11 | Verkehr: Antrieb/Abrieb/Verdunstung | TRANSPORT_TRINITY | [List](descriptors/traffic_trinity.csv) |
| Vehicle type | 12 | Verkehr: Fahrzeugtyp | VEHICLE_TYPE | [List](descriptors/vehicles.csv) |
| Euro norm | 13 | Verkehr: Euro-Norm | EURO_NORM | [List](descriptors/euro_norm.csv) |
| Product | 14 | Industrie: Produkt | PRODUCT | [List](descriptors/product.csv) |
| Species | 15 | Landwirtschaft: Tierart | SPECIES | [List](descriptors/species.csv) |
| Manure management | 16 | Landwirtschaft: Güllemanagement | MANURE_MANAGEMENT | [List](descriptors/manure_management.csv) |
| Land use category changed from | 17 | LULUCF: Von Nutzung | LUC_FROM | [List](descriptors/land_use_categories_from.csv) |
| Land use category changed to | 18 | LULUCF: Zu Nutzung | LUC_TO | [List](descriptors/land_use_categories_to.csv) |
| Pool | 19 | LULUCF: Pool | POOL | [List](descriptors/pools.csv) |
