import csv
import glob
import xml.etree.ElementTree as ET

ZEROS = '0.00000000'
MISSING_UID_PLACEHOLDER = 'MISSING_UID!'

replacement_dict = {}
for filepath in glob.glob(f'**/replace_zeros.csv', recursive=True):
    with open(filepath, mode='r') as in_file:
        replacement_dict.update({rows[0]:rows[1] for rows in csv.reader(in_file, delimiter=';')})

for filepath in glob.glob('**/*.xml', recursive=True):
    tree = ET.parse(filepath)
    
    for var in tree.findall('.//variable'):
        uid = var.get('uid')
        for value in var.findall('.//year/record/value'):
            if value.text is None or value.text == ZEROS or value.text == MISSING_UID_PLACEHOLDER :
                # Note: for some UIDs the replacement will be empty which might be fine,
                # but we do want to flag the cases where we cannot find the UID.
                value.text = replacement_dict.get(uid, MISSING_UID_PLACEHOLDER)

    tree.write(filepath)
