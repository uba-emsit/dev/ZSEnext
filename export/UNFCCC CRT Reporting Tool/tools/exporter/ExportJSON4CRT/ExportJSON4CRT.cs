﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using System.IO.Compression;
using System.Text;
using UBA.Mesap;

namespace ExportJSON4CRT;

internal class ExportJSON4CRT
{
    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([Export], "ZSE CRT Exporter");
    }

    private static void Export(Database db)
    {
        EventInventory variables = db.EventInventories["crf_variables"];

        EventFilter eventFilter;
        EventFilterFactory eventFilterFactory = variables.CreateFilterFactory(db.DatabaseUserContext);

        string UID = "EEFCAFB4-0579-4DC8-828B-AAD277318325";
        if (eventFilterFactory.TryCreateFilter($"uid = '{UID}'", out eventFilter))
        {
            var reader = new EventReader(db.DatabaseUserContext);
            if (reader.Read(variables, eventFilter).FirstOrDefault() is { } variable)
            {
                Console.WriteLine($"Checking variable with ID {UID}:");
                EventValueMemo value = (EventValueMemo) variable["filter_settings"];
                TimeseriesFilterSettings timeseriesFilter = DecodeFilterSetting(db, value.Value);

                var timeseriesReader = new TimeseriesReader(db.DatabaseUserContext);
                List<Timeseries> timeseries = timeseriesReader.Read(timeseriesFilter);
                Console.WriteLine($"Found filter with {timeseries.Count} time series!");

                var AggregationSpecification = new AggregationSpecification
                {
                    AggregationAllowIncompleteSpecification = AggregationAllowIncompleteSpecification.On,
                    AggregationMappingModeSpecification = AggregationMappingModeSpecification.CompleteMapping,
                    AggregationRuleSpecification = AggregationRuleSpecification.Sum,
                    MappingSpecification = MappingSpecification.TimeseriesSetting
                };
                var startDate = new DateTimeOffset(1990, 1, 1, 0, 0, 0, TimeSpan.FromHours(1));
                var endDate = new DateTimeOffset(2020, 12, 31, 0, 0, 0, TimeSpan.FromHours(1));
                TimeAxisAbsolute timeAxis = TimeAxis.CreateAbsolute(startDate, endDate);
                var timeseriesDataReader = new TimeseriesDataReader(db.DatabaseUserContext);
                var result = timeseriesDataReader.ReadAggregatedAcrossTimeseries(timeseries,
                    db.Hypotheses["REF"], timeAxis, Mesap.Framework.Common.TimeResolution.Year,
                    TimezoneSpecification.DatabaseSetting, AggregationSpecification, "TJ");

                foreach (var aggregation in result)
                    Console.WriteLine($"Aggregation result: {aggregation.Key}: {aggregation.Value}");
            }
        } else
            Console.WriteLine($"Variable with ID {UID} not found!");
    }

    private static TimeseriesFilterSettings DecodeFilterSetting(Database db, string compressedFilterSettings)
    {
        byte[] compressedData = Convert.FromBase64String(compressedFilterSettings);

        string decompressedFilterSettings;
        using (var inputStream = new MemoryStream(compressedData))
        using (var gzipStream = new GZipStream(inputStream, CompressionMode.Decompress))
        using (var streamReader = new StreamReader(gzipStream, Encoding.UTF8))
        {
            decompressedFilterSettings = streamReader.ReadToEnd();
        }

        return new TimeseriesFilterSettingsSerializer(db.DatabaseUserContext).Deserialize(decompressedFilterSettings);
    }
}
