﻿using Mesap.Framework;
using Mesap.Framework.DataAccess.Tables.Internal;
using Mesap.Framework.Entities;
using System.Text;
using UBA.Mesap;

namespace UBA.CubeManager;

internal class ExportTrees
{
    private const string ScriptFilePathPrefix = "../../../../../trees/";

    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([ExportDescriptorTrees], appName: "ZSE Cube Manager", runChecks: false);
    }

    private static void ExportDescriptorTrees(Database database)
    {
        foreach (Tree tree in database.Trees)
        {
            EntityTree<TreeNode, int> descriptorTree = EntityTree.Create(
                tree.Structure.Select(node => node.Entity).OrderBy(descriptor => descriptor.LevelSortNr).ToList(),
                node => node.NodePredecessorNr);

            DirectoryInfo folder = Directory.CreateDirectory($"{ScriptFilePathPrefix}{tree.Dimension.Id}");
            using (FileStream file = File.Open($"{folder.FullName}/{tree.Id}.tree", FileMode.Create))
            using (StreamWriter outputFile = new(file, new UTF8Encoding(false)))
            {
                descriptorTree.RootNode.VisitChildren(descriptor =>
                {
                    outputFile.WriteLine($"{new string('\t', descriptor.Level)}{descriptor.Entity.Descriptor.Id} ({descriptor.Entity.Descriptor.Name})");
                }, true);
            }

            Console.WriteLine($"Wrote structure of tree \"{tree.Name}\" to {folder.FullName}\\{tree.Id}.tree");
        }
    }
}
