﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using System.Collections.Immutable;
using System.Text;
using UBA.Mesap;

namespace UBA.CalculationManager;

internal class ExportCalculationMethods
{
    private const string ScriptFilePathPrefix = "../../../../data/";

    static void Main(string[] args)
    {
        ScriptHelper.ConnectAndExecute([ExportScripts], appName: "ZSE Script Manager", runChecks: false);
    }

    private static void ExportScripts(Database db)
    {
        CalculationMethodReader reader = new(db.DatabaseUserContext);
        TimeseriesReader timeseriesReader = new(db.DatabaseUserContext);

        // Find and export all calculation scripts
        reader.ReadTree().RootNode.VisitChildren(script =>
        {
            if (script.Entity.IsLeaf)
            {
                string path = string.Empty;
                script.VisitParents(parent => path = parent.Level > 0 ? $"{parent.Entity.Id}/{path}" : path, false);

                DirectoryInfo folder = Directory.CreateDirectory($"{ScriptFilePathPrefix}{path}");
                using (FileStream file = File.Open($"{folder.FullName}{script.Entity.Id}.calc", FileMode.Create))
                using (StreamWriter outputFile = new(file, new UTF8Encoding(false)))
                {
                    CalculationMethod method = reader.Read([script.Entity.Id])[0];

                    // Write variable info, incl. time series IDs and names
                    IReadOnlyList<CalculationVariable> variables = method.Variables;
                    List<Timeseries> allSeries = timeseriesReader.Read(variables.Select(var => var.TsNr));
                    foreach (CalculationVariable variable in variables.OrderBy(var => var.VariableType).ThenBy(var => var.Name))
                    {
                        outputFile.WriteLine($"{variable.VariableType} {variable.Name}: {GetTimeseriesInfo(allSeries, variable)}");
                    }

                    // Write calculation script body
                    outputFile.WriteLine();
                    outputFile.WriteLine(method.EquationText.ReplaceLineEndings("\r\n"));
                }

                Console.WriteLine($"Wrote script \"{script.Entity.Name}\" to {folder.FullName}{script.Entity.Id}.calc");
            }
        }, false);
    }

    private static string GetTimeseriesInfo(List<Timeseries> allSeries, CalculationVariable variable)
    {
        string tsInfo;
        if (variable.TsNr > 0)
        {
            Timeseries? timeseries = allSeries.Find(var => var.PrimaryKey == variable.TsNr);
            tsInfo = timeseries != null ?
                $"{timeseries.Id} ({timeseries.Name})" :
                $"Linked to non-existent time series with key {variable.TsNr}";
        }
        else
        {
            tsInfo = "Loop";
        }

        return tsInfo;
    }
}
